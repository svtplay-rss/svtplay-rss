#!/usr/bin/python
# -*- coding: utf-8 -*-

from BeautifulSoup import BeautifulSoup
import urllib2
import datetime
import dateutil.parser

baseurl = 'http://www.svtplay.se'

def getPrograms():
    url = baseurl + '/program'

    ret = u"""<h1>Available RSS feeds:</h1><br/>
<h2>Categories:</h2>
<a href=svtplay-rss.py?show=kids&category=1>Barn</a><br/>
<a href=svtplay-rss.py?show=documentary&category=1>Dokumentär</a><br/>
<a href=svtplay-rss.py?show=filmAndDrama&category=1>Film &amp; Drama</a><br/>
<a href=svtplay-rss.py?show=cultureAndEntertainment&category=1>Kultur &amp; Nöje</a><br/>
<a href=svtplay-rss.py?show=news&category=1>Nyheter</a><br/>
<a href=svtplay-rss.py?show=societyAndFacts&category=1>Samhälle &amp; Fakta</a><br/>
<a href=svtplay-rss.py?show=sport&category=1>Sport</a><br/>
<a href=svtplay-rss.py?show=openArchive&category=1>Öppet arkiv</a><br/>
"""
    
    try:
        response = urllib2.urlopen(url)
        html = response.read()
        
    except (URLError, HTTPError):
        return "Could not open we page: " + url
        
    try:
        soup = BeautifulSoup(html)
        ret += '\n<h2>Programs:</h2><br/>\n'
        for a in soup.findAll('a', {'class' : 'playAlphabeticLetterLink'}):
            ret += '<a href=svtplay-rss.py?show=%s&videos=1&klipp=1>%s</a><br/>\n' % (a['href'][1:], a.contents[0])
            
    except (ValueError, IndexError, IOError):
        return "No such place found: "
    return ret

    
def getProgramEpisodes(programId, videos, klipp, category):
    now = datetime.datetime.now()
    pubdate = now.strftime("%a, %d %b %Y %H:%M:%S +0100")
    
    html = ''
    if videos and not category:
        url = baseurl + '/ajax/videos?title=%s&context=title' % programId
        if True:
            response = urllib2.urlopen(url)
            html = response.read()
    
    if klipp and not category:
        url = baseurl + '/ajax/klipp?title=%s&context=title' % programId
        if True:
            response = urllib2.urlopen(url)
            html += response.read()
    
    if category:
        url = baseurl + '/ajax/videos?category=%s' % programId
        if True:
            response = urllib2.urlopen(url)
            html += response.read()
    
    ret = """<?xml version="1.0" encoding="UTF-8" ?>
<rss version="2.0">
<channel>
    <title>Unofficial SVT Play RSS feed</title>
    <description>This is a home-made RSS feed scraped from svtplay.se</description>
    <link>%s</link>
    <lastBuildDate>%s</lastBuildDate>
    <pubDate>%s</pubDate>
    <ttl>600</ttl>
    """ % (baseurl + '/' + ('program' if category else programId), pubdate, pubdate)
        
    #except (URLError, HTTPError):
        #return "Could not open we page: " + url
        
    if True:
        soup = BeautifulSoup(html, convertEntities=BeautifulSoup.HTML_ENTITIES)
        for art in soup.findAll('article'):
            template = u"""
    <item>
        <title>%s</title>
        <description>%s</description>
        <link>%s</link>
        <guid>%s</guid>
        <pubDate>%s</pubDate>
    </item>
            """
            link = art.find('a', {'class' : 'playLink playBoxWithClickArea playIELinkFix'})['href']
            img = art.find('img', {'class' : 'playGridThumbnail'})['src']
            realdate = art.find('time')['datetime']
            pubDate = dateutil.parser.parse(realdate)
            
            pubDate = pubDate.strftime("%a, %d %b %Y %H:%M:%S +0100")
            
            ret += template % (art['data-title'], art['data-description'], baseurl + link, baseurl + link, pubDate)
            
    #except (ValueError, IndexError, IOError):
        #return "No such place found: "
        
    ret += """
</channel>
</rss>
    """
    return ret
        
    
def index(req):
    show = req.form.getfirst('show', '')
    klipp = req.form.getfirst('klipp', '')
    videos = req.form.getfirst('videos', '')
    category = req.form.getfirst('category', '')
    if  not klipp and not videos:
        klipp = '1'
        videos = '1'
    r = 'broken'
    if show:
        req.content_type = 'text/xml;charset=utf-8'
        r = getProgramEpisodes(show, videos, klipp, category)
    else:
        req.content_type = 'text/html;charset=utf-8'
        r = getPrograms()
    r = r.encode('utf-8')
    return r
    
    
#print getProgramEpisodes('dinosaurietaget').encode('utf-8')