<?php

require('simple_html_dom.php');

$baseurl = 'http://www.svtplay.se';

if ($_GET['show'] or $_GET['category_rss'])
{
  header('Content-type: application/xml');
?>
<?xml version="1.0" encoding="UTF-8" ?>
<rss version="2.0">
<channel>
  <title>SvtPlay RSS</title>
  <description>SvtPlay RSS</description>
  <link>http://www.svtplay.se</link>
  <lastBuildDate>Mon, 06 Sep 2010 00:01:00 +0000 </lastBuildDate>
  <pubDate><?php echo date("D, d M Y H:i:s O"); ?></pubDate>
  <ttl>1800</ttl>
<?php
  if ($_GET['show'])
    $html = file_get_html($baseurl.'/ajax/videos?title='.$_GET['show'].'&antal=0');
  else if ($_GET['category_rss'])
    $html = file_get_html($baseurl.'/ajax/videos?category='.$_GET['category_rss'].'&antal=0');
  

  foreach($html->find('article') as $element)
  {
    $url = $element->find('a', 0)->href;
    $url = $baseurl.'/'.ltrim($url, '/');
    $img = $element->find('img', 0)->getAttribute('src');
    $title = $element->getAttribute('data-title');
    $desc = $element->getAttribute('data-description');
    $desc = $element->getAttribute('data-description');
    $len = $element->getAttribute('data-length');
    $time = $element->find('time', 0)->datetime;
    $pubDate= date("D, d M Y H:i:s O", strtotime($time));

    $description = "<img src=\"$img\"></img><br />$desc<br />($len)<br /><a href=\"http://www.pirateplay.se/app.html#$url\">PiratePlay</a>";
?>
 <item>
  <title><?php echo html_entity_decode($title); ?></title>
  <description><![CDATA[<?php echo $description; ?>]]></description>
  <link><?php echo $url; ?></link>
  <guid><?php echo $url; ?></guid>
  <pubDate><?php echo $pubDate ?></pubDate>
 </item>
<?php
  }
?>
</channel>
</rss>
<?php
}
else
{
?>

<html>
<head><title>SvtPlay RSS feeds</title></head>
<body>
  <h1>Available RSS feeds</h1>
<?php

  if ($_GET['category'])
  {
    echo '  <h2>Shows:</h2>';
    $html = file_get_html($baseurl.'/ajax/program?category='.$_GET['category'].'&antal=0');
  
    foreach($html->find('article') as $element)
    {
      $url = $element->find('a', 0)->href;
      $url = ltrim($url, '/');
      $title = $element->getAttribute('data-title');
      $desc = $element->getAttribute('data-description');

      echo '  <p><a href="svtplay-rss.php?show='.$url.'">'.$title."</a><br />\n  &nbsp;&nbsp;".$desc."<p />\n";
    }
  }
  else
  {
?>
  <h2>Categories:</h2>
  <a href="svtplay-rss.php?category=kids">Barn</a> (<a href="svtplay-rss.php?category_rss=kids">RSS</a>)<br/>
  <a href="svtplay-rss.php?category=documentary">Dokument&auml;r</a> (<a href="svtplay-rss.php?category_rss=documentary">RSS</a>)<br/>
  <a href="svtplay-rss.php?category=filmAndDrama">Film &amp; Drama</a> (<a href="svtplay-rss.php?category_rss=filmAndDrama">RSS</a>)<br/>
  <a href="svtplay-rss.php?category=cultureAndEntertainment">Kultur &amp; N&ouml;je</a> (<a href="svtplay-rss.php?category_rss=cultureAndEntertainment">RSS</a>)<br/>
  <a href="svtplay-rss.php?category=news">Nyheter</a> (<a href="svtplay-rss.php?category_rss=news">RSS</a>)<br/>
  <a href="svtplay-rss.php?category=societyAndFacts">Samh&auml;lle &amp; Fakta</a> (<a href="svtplay-rss.php?category_rss=societyAndFacts">RSS</a>)<br/>
  <a href="svtplay-rss.php?category=sport">Sport</a> (<a href="svtplay-rss.php?category_rss=sport">RSS</a>)<br/>
<?php
  }
?>
</body>
</html>
<?php
}

?>

